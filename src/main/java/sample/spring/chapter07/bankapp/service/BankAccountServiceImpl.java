package sample.spring.chapter07.bankapp.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sample.spring.chapter07.bankapp.dao.BankAccountDao;
import sample.spring.chapter07.bankapp.domain.BankAccountDetails;

@Service(value = "bankAccountService")
public class BankAccountServiceImpl implements BankAccountService {

	@Autowired
	private BankAccountDao bankAccountDao;

	@Override
	@Transactional
	public int createBankAccount(BankAccountDetails bankAccountDetails) {
		return bankAccountDao.createBankAccount(bankAccountDetails);
	}

	@Override
	@Transactional
	public void transferMoney(int bankAccountSourceId, int bankAccountDestinationId, double amount) {
		BankAccountDetails source = bankAccountDao.getBankAccount(bankAccountSourceId);
		BankAccountDetails destination = bankAccountDao.getBankAccount(bankAccountDestinationId);

		if (source.getBalanceAmount() < amount) {
			throw new RuntimeException(
					"Insufficient balance amount in source bank account");
		}

		source.setBalanceAmount(source.getBalanceAmount() - (int)amount);
		destination.setBalanceAmount(destination.getBalanceAmount() + (int)amount);

	}

}
